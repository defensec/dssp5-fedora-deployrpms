# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

.PHONY: all clean install uninstall

PREFIX ?= /usr
DESTDIR ?=
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
BASHCOMPDIR ?= $(PREFIX)/share/bash-completion/completions

ifeq ($(WITH_BASHCOMP),)
ifneq ($(strip $(wildcard $(BASHCOMPDIR))),)
WITH_BASHCOMP := yes
endif
endif

all: clean
	argbash -o dssp5-fedora-deployrpms.sh dssp5-fedora-deployrpms.sh
	argbash --type completion --strip all \
	-o dssp5-fedora-deployrpms.bash-completion dssp5-fedora-deployrpms.sh
	argbash --type manpage-defs --strip all \
	-o dssp5-fedora-deployrpms-defs.rst dssp5-fedora-deployrpms.sh
	sed -i \
	's/<Your name goes here>/Dominick Grift <dominick.grift@defensec.nl>/' \
	dssp5-fedora-deployrpms-defs.rst
	sed -i -e '/<More elaborate description/{r EXAMPLES' \
	-e 'd}' dssp5-fedora-deployrpms-defs.rst
	sed -i '/[[:space:]][[:space:]][[:space:]]goes here>./d' \
	dssp5-fedora-deployrpms-defs.rst
	argbash --type manpage --strip all -o dssp5-fedora-deployrpms.rst \
	dssp5-fedora-deployrpms.sh
	rst2man dssp5-fedora-deployrpms.rst > dssp5-fedora-deployrpms.1

clean:
	$(RM) dssp5-fedora-deployrpms.bash-completion dssp5-fedora-deployrpms-defs.rst \
	dssp5-fedora-deployrpms.rst dssp5-fedora-deployrpms.1

install: all
	@install -v -d "$(DESTDIR)$(MANDIR)/man1" && install \
	-m 0644 -v dssp5-fedora-deployrpms.1 \
	"$(DESTDIR)$(MANDIR)/man1/dssp5-fedora-deployrpms.1"
	@[ "$(WITH_BASHCOMP)" = "yes" ] || exit 0; install -v \
	-d "$(DESTDIR)$(BASHCOMPDIR)" && install -m 0644 \
	-v dssp5-fedora-deployrpms.bash-completion \
	"$(DESTDIR)$(BASHCOMPDIR)/dssp5-fedora-deployrpms"
	install -v -d "$(DESTDIR)$(BINDIR)/" && install -m 0755 \
	-v dssp5-fedora-deployrpms.sh "$(DESTDIR)$(BINDIR)/dssp5-fedora-deployrpms"

uninstall:
	@rm -vf \
	"$(DESTDIR)$(BINDIR)/dssp5-fedora-deployrpms" \
	"$(DESTDIR)$(MANDIR)/man1/dssp5-fedora-deployrpms.1" \
	"$(DESTDIR)$(BASHCOMPDIR)/dssp5-fedora-deployrpms"
